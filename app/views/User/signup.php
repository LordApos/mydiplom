<div class="register-logo">
    <a><b>Bug</b>Tracker</a>
</div>

<div class="card">
    <div class="card-body register-card-body">
        <p class="login-box-msg">Регистрация</p>

        <form action="/user/signup" method="post" data-toggle="validator">
            <div class="input-group mb-3 form-group has-feedback">
                <input type="text" class="form-control" placeholder="Логин" name="login">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-user"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Имя" name="firstname">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-file-signature"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Фамилия" name="secondname">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-signature"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input type="email" class="form-control" placeholder="Email" name="email">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input type="password" class="form-control" placeholder="Пароль" name="password">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- /.col -->
                <div class="col-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Зарегистрироватся</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        <a href="/user/login" class="btn btn-secondary btn-block btn-flat" style="margin-top: 0.5rem;">Вы зарегистрируваные? Войти</a>
    </div>
</div>

<!-- /.form-box -->