<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">

                        <h3 class="profile-username text-center"><?=$user['firstname'];?> <?=$user['secondname'];?></h3>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Задач</b> <a class="float-right"><?=$alltasks;?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Завершенных задач</b> <a class="float-right"><?=$finishtasks;?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Активных задач</b> <a class="float-right"><?=$activetasks;?></a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- About Me Box -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Обо мне</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <strong><i class="fas fa-book mr-1"></i> Професия</strong>

                        <p class="text-muted text-uppercase">
                            <?=$user['profession'];?>
                        </p>

                        <hr>
                        <?php if (!empty($atts)):?>
                        <strong><i class="fas fa-pencil-alt mr-1"></i> Метки</strong>

                        <p class="text-muted">
                            <?php foreach ($atts as $att):?>
                            <span class="tag"><?=$att['name'];?></span>
                            <?php endforeach;?>
                        </p>

                        <hr>
                        <?php endif;?>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Настройки</a></li>
                        </ul>
                    </div><!-- /.card-header -->
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="settings">
                                <form class="form-horizontal" method="post" action="/user/edit">
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-2 col-form-label">Имя</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="firstname" value="<?=$user['firstname'];?>" class="form-control" id="inputName" placeholder="Имя">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-2 col-form-label">Фамилия</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="secondname" value="<?=$user['secondname'];?>" class="form-control" id="inputName" placeholder="Имя">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" value="<?=$user['email'];?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName2" class="col-sm-2 col-form-label">Пароль</label>
                                        <div class="col-sm-10">
                                            <input type="password" name="password" class="form-control" id="inputName2" placeholder="Пароль">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-danger">Изменить</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div><!-- /.card-body -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>