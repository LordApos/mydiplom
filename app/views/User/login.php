<div class="login-logo">
    <a><b>Bug</b>Tracker</a>
</div>
<!-- /.login-logo -->
<div class="card">
    <div class="card-body login-card-body">
        <p class="login-box-msg">Авторизация</p>

        <form action="/user/login" method="post">
            <div class="input-group mb-3">
                <input required type="text" value="lordapos" name="login" class="form-control" placeholder="Логин">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-user"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input required type="password" value="123456" name="password" class="form-control" placeholder="Пароль">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Войти</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <a href="/user/signup" class="btn btn-secondary btn-block btn-flat" style="margin-top: 0.5rem;">Зарегистрироватся</a>
    </div>
    <!-- /.login-card-body -->
</div>
