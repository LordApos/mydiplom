<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <?= $this->getMeta(); ?>
    <link rel="stylesheet" href="/assets/style/vendor/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="/assets/style/vendor/adminlte.min.css">
    <link href='/assets/style/vendor/fullcalendar.min.css' rel='stylesheet' />
    <link href='/assets/style/vendor/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <link rel="stylesheet" href="/assets/style/components/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="/assets/images/logo.png" rel="icon" />
</head>
<body class="hold-transition sidebar-mini">
<script src='/assets/js/vendor/moment.min.js'></script>
<!-- jQuery -->
<script src="/assets/js/vendor/jquery.min.js"></script>
<!--fullcalendar-->
<script src='/assets/js/vendor/fullcalendar.min.js'></script>
<!-- Bootstrap 4 -->
<script src="/assets/js/vendor/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/assets/js/vendor/adminlte.min.js"></script>
<script src="/assets/js/custom/custom.js"></script>
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <?php if ($_SESSION['user']['role'] == 'admin'):?>
                <li class="nav-item">
                    <a class="nav-link" href="/admin"><i class="fas fa-tools"></i></a>
                </li>
            <?php endif;?>
            <li class="nav-item">
                <a class="nav-link" href="/user/logout"><i class="fas fa-sign-out-alt"></i></a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="/" class="brand-link">
            <img src="/assets/images/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8; background-color: white;">
            <span class="brand-text font-weight-light">BugTracker</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="info">
                    <a href="#" class="d-block"><?=$_SESSION['user']['secondname'];?> <?=$_SESSION['user']['firstname'];?></a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="/" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Головна
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-tasks"></i>
                            <p>
                                Список задач
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="/tasks/myprojects" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Мои задачи</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/tasks" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Все задачи</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="/user/cabinet" class="nav-link">
                            <i class="nav-icon fas fa-user"></i>
                            <p>
                                Профиль
                                <span class="right badge badge-danger">New</span>
                            </p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12">
                <?php if(isset($_SESSION['error'])): ?>
                    <div class="alert alert-danger">
                        <?php echo $_SESSION['error']; unset($_SESSION['error']); ?>
                    </div>
                <?php endif; ?>
                <?php if(isset($_SESSION['success'])): ?>
                    <div class="alert alert-success">
                        <?php echo $_SESSION['success']; unset($_SESSION['success']); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <section class="content">
            <?=$content; ?>
        </section>
    </div>
    <!-- /.content-wrapper -->
    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
            Awesome for you
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2014-2019 BugTracker.</strong> All rights reserved.
    </footer>
</div>
<!-- ./wrapper -->
<?php
use RedBeanPHP\R;
$logs = R::getDatabaseAdapter()
    ->getDatabase()
    ->getLogger();

//debug( $logs->grep( 'SELECT' ) );
?>
<!-- REQUIRED SCRIPTS -->




</body>
</html>
