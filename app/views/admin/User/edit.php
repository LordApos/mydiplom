<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="<?= ADMIN; ?>/user/edit?id=<?= $user['id']; ?>" method="post">
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <label for="profession">Профессия</label>
                            <select required name="profession" id="profession" class="col-2">
                                <option <?php if ($user['profession'] == 'backend')
                                {
                                    echo 'selected';
                                } ?> value="backend">BackEnd
                                </option>
                                <option <?php if ($user['profession'] == 'designer')
                                {
                                    echo 'selected';
                                } ?> value="designer">Дизайнер
                                </option>
                                <option <?php if ($user['profession'] == 'frontend')
                                {
                                    echo 'selected';
                                } ?> value="frontend">FrontEnd
                                </option>
                                <option <?php if ($user['profession'] == 'newuser')
                                {
                                    echo 'selected';
                                } ?> value="newuser">Участник
                                </option>
                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <p for="raiting">Качество работы</p>
                            <div class="raiting__block">
                                <div>
                                    <input <?php if ($user['raiting'] == 1) echo 'checked';?> type="radio" id="1star" name="raiting" value="1">
                                    <label for="1star">1</label>
                                </div>
                                <div>
                                    <input <?php if ($user['raiting'] == 2) echo 'checked';?> type="radio" id="2star" name="raiting" value="2">
                                    <label for="2star">2</label>
                                </div>
                                <div>
                                    <input <?php if ($user['raiting'] == 3) echo 'checked';?> type="radio" id="3star" name="raiting" value="3">
                                    <label for="3star">3</label>
                                </div>
                                <div>
                                    <input <?php if ($user['raiting'] == 4) echo 'checked';?> type="radio" id="4star" name="raiting" value="4">
                                    <label for="4star">4</label>
                                </div>
                                <div>
                                    <input <?php if ($user['raiting'] == 5) echo 'checked';?> type="radio" id="5star" name="raiting" value="5">
                                    <label for="5star">5</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="role">Доступы</label>
                            <select class="col-2" name="role" id="role">
                                <option <?php if ($user['role'] == 'user') echo 'selected';?> value="user">Пользователь</option>
                                <option <?php if ($user['role'] == 'admin') echo 'selected';?> value="admin">Админ</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->