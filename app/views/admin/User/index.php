<div class="card">
    <div class="card-header">
        <h3 class="card-title">Все пользователи</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
        </div>
    </div>
    <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
            <tr>
                <th style="width: 2%">
                    #
                </th>
                <th style="width: 15%">
                    Имя Фамилия
                </th>
                <th style="width: 15%">
                    Логин
                </th>
                <th style="width: 20%">
                    Email
                </th>
                <th style="width: 15%">
                    Професия
                </th>
                <th style="width: 20%">

                </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $item):?>
                <tr>
                    <td>
                        <?=$item['id'];?>
                    </td>
                    <td>
                        <a>
                            <?=$item['firstname'];?> <?=$item['secondname'];?>
                        </a>
                    </td>
                    <td>
                        <a>
                            <?=$item['login'];?>
                        </a>
                    </td>
                    <td>
                        <a href="mail:<?=$item['email'];?>">
                            <?=$item['email'];?>
                        </a>
                    </td>
                    <td>
                        <a>
                            <?=$item['profession'];?>
                        </a>
                    </td>
                    <td class="project-actions text-right">
                        <a class="btn btn-info btn-sm" href="<?=ADMIN;?>/user/edit?id=<?=$item['id'];?>">
                            <i class="fas fa-pencil-alt"></i>
                        </a>
                        <a class="btn btn-danger btn-sm" href="<?=ADMIN;?>/user/delete?id=<?=$item['id'];?>">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>