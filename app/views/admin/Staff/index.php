<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <form action="<?= ADMIN; ?>/staff" method="post">
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <label for="task">Задача</label>
                            <select name="task" id="task">
                                <?php foreach ($tasks as $task): ?>
                                    <option value="<?= $task['id']; ?>"><?= $task['title']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">Подобрать</button>
                    </div>
                </form>
            </div>
        </div>
        <?php if (!empty($usersRaiting)): ?>
            <div class="col-md-6">
                <form class="change__dev__form" action="<?= ADMIN; ?>/staff/edit" method="post">
                    <input type="hidden" name="id" value="<?=$taskID;?>">
                    <?php foreach ($usersRaiting as $item):?>
                    <div class="devs__item">
                        <input required id="user<?=$item['id'];?>" type="radio" name="dev_id" value="<?=$item['id'];?>">
                        <label for="user<?=$item['id'];?>"><?=$item['firstname'];?> <?=$item['secondname'];?>
                            <br>
                            Рейтинг: <?=$item['allraiting'];?>
                            <br>
                            Активных задач: <?=$item['activetask'];?>
                        </label>
                    </div>
                    <?php endforeach;?>
                    <button class="btn btn-success" type="submit">Выбрать</button>
                </form>
            </div>
        <?php endif; ?>
    </div>
</section>
<!-- /.content -->