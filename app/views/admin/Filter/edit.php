<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="<?=ADMIN;?>/filter/edit?id=<?=$attributs['id'];?>" method="post">
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <label for="title">Наименование</label>
                            <input type="text" name="name" class="form-control" id="title" placeholder="Наименование" value="<?=h($attributs['name']);?>" required>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <label for="profession">Профессия</label>
                            <select required name="profession" id="profession">
                                <option <?php if ($attributs['profession'] == 'backend') echo 'selected';?> value="backend">BackEnd</option>
                                <option <?php if ($attributs['profession'] == 'frontend') echo 'selected';?> value="frontend">FrontEnd</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->