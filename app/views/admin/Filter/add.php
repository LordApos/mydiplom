<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="<?=ADMIN;?>/filter/add" method="post">
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <label for="title">Наименование</label>
                            <input type="text" name="name" class="form-control" id="title" placeholder="Наименование" value="" required>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="profession">Профессия</label>
                            <select required name="profession" id="profession">
                                <option value="backend">BackEnd</option>
                                <option value="frontend">FrontEnd</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->