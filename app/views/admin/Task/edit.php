<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="<?=ADMIN;?>/task/edit?id=<?=$task['id'];?>" method="post">
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <label for="title">Наименование задачи</label>
                            <input type="text" name="title" class="form-control" id="title" placeholder="Наименование задачи" value="<?=h($task['title']);?>" required>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="editor1">Контент</label>
                            <textarea name="content" id="editor1" cols="80" rows="10"><?=$task['content'];?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="atts">Метки</label>
                            <select name="atts[]" class="form-control select2" id="atts" multiple>
                                <?php if(!empty($atts)): ?>
                                    <?php foreach($atts as $item): ?>
                                        <?php if ($item['status'] == 1): ?>
                                            <option selected value="<?= $item['id']; ?>"><?= $item['name']; ?></option>
                                        <?php else:?>
                                            <option value="<?= $item['id']; ?>"><?= $item['name']; ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="deadline">Дедлайн</label>
                            <input type="date" id="deadline" name="deadline" value="<?= $task['deadline']; ?>">
                        </div>

                        <div class="form-group has-feedback">
                            <label for="status">Статус</label>
                            <select id="status" class="col-6 task__changeStatus" name="status">
                                <option value="2" <?php if ($task['status'] == 2) echo 'selected';?>>Завершено</option>
                                <option value="1" <?php if ($task['status'] == 1) echo 'selected';?>>В процессе</option>
                                <option value="0" <?php if ($task['status'] == 0) echo 'selected';?>>Не начата</option>
                            </select>
                        </div>

                        <div class="form-group has-feedback">
                            <p for="raiting">Качество работы</p>
                            <div class="raiting__block">
                                <div>
                                    <input <?php if ($task['rating'] == 1) echo 'checked';?> type="radio" id="1star" name="rating" value="1">
                                    <label for="1star">1</label>
                                </div>
                                <div>
                                    <input <?php if ($task['rating'] == 2) echo 'checked';?> type="radio" id="2star" name="rating" value="2">
                                    <label for="2star">2</label>
                                </div>
                                <div>
                                    <input <?php if ($task['rating'] == 3) echo 'checked';?> type="radio" id="3star" name="rating" value="3">
                                    <label for="3star">3</label>
                                </div>
                                <div>
                                    <input <?php if ($task['rating'] == 4) echo 'checked';?> type="radio" id="4star" name="rating" value="4">
                                    <label for="4star">4</label>
                                </div>
                                <div>
                                    <input <?php if ($task['rating'] == 5) echo 'checked';?> type="radio" id="5star" name="rating" value="5">
                                    <label for="5star">5</label>
                                </div>
                            </div>
                        </div>

                        <div class="change__dev">
                            <label for="dev">Разработчик</label>
                            <select id="dev" class="col-6 task__changeStatus" name="dev_id">
                                <option value=""></option>
                                <?php foreach ($devs as $dev):?>
                                <option <?php if ($task['dev_id'] == $dev['id']) echo 'selected';?> value="<?=$dev['id'];?>"><?=$dev['firstname'];?> <?=$dev['secondname'];?> <?=$dev['profession'];?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="hidden" name="id" value="<?=$task['id'];?>">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->