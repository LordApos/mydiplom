<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="<?=ADMIN;?>/task/add" method="post">
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <label for="title">Наименование задачи</label>
                            <input type="text" name="title" class="form-control" id="title" placeholder="Наименование задачи" value="" required>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="editor1">Контент</label>
                            <textarea required name="content" id="editor1" cols="80" rows="10"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="atts">Метки</label>
                            <select required name="atts[]" class="form-control select2" id="atts" multiple>
                                <?php if(!empty($atts)): ?>
                                    <?php foreach($atts as $item): ?>
                                        <option value="<?= $item['id']; ?>"><?= $item['name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>

                        <div class="form-group has-feedback ">
                            <label for="deadline">Дедлайн</label>
                            <input required min="<?= date('Y-m-d'); ?>" class="col-6" type="date" id="deadline" name="deadline" value="">
                        </div>

                        <div class="form-group has-feedback">
                            <label for="status">Статус</label>
                            <select id="status" class="col-6 task__changeStatus" name="status">
                                <option value="2">Завершено</option>
                                <option value="1">В процессе</option>
                                <option value="0" selected>Не начата</option>
                            </select>
                        </div>
                        <div class="change__dev">
                            <label for="dev">Разработчик</label>
                            <select id="dev" class="col-6 task__changeStatus" name="dev_id">
                                <option value="" selected></option>
                                <?php foreach ($devs as $dev):?>
                                    <option value="<?=$dev['id'];?>"><?=$dev['firstname'];?> <?=$dev['secondname'];?> <?=$dev['profession'];?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->