<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="<?=ADMIN;?>/events/add" method="post">
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <label for="title">Наименование</label>
                            <input type="text" name="title" class="form-control" id="title" placeholder="Наименование" value="" required>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="start">Начало</label>
                            <input type="date" id="start" name="start" value="" required>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="end">Конец (если пустой - то мероприятие целый день)</label>
                            <input type="date" id="end" name="end" value="">
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->