<div class="card">
    <div class="card-header">
        <h3 class="card-title">Все мероприятия</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
        </div>
    </div>
    <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
            <tr>
                <th style="width: 10%">
                    #
                </th>
                <th style="width: 50%">
                    Название
                </th>
                <th style="width: 30%">
                    Дата
                </th>
                <th style="width: 10%">

                </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($events as $event):?>
                <tr>
                    <td>
                        <?=$event['id'];?>
                    </td>
                    <td>
                        <a>
                            <?=$event['title'];?>
                        </a>
                    </td>
                    <td>
                        <span class="date__start"><?=$event['start'];?></span>
                        <?php if (!empty($event['end'])):?>
                        <span class="date__start"> - <?=$event['end'];?></span>
                        <?php endif;?>
                    </td>
                    <td class="project-actions text-right">
                        <a class="btn btn-info btn-sm" href="<?=ADMIN;?>/events/edit?id=<?=$event['id'];?>">
                            <i class="fas fa-pencil-alt"></i>
                        </a>
                        <a class="btn btn-danger btn-sm" href="<?=ADMIN;?>/events/delete?id=<?=$event['id'];?>">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>