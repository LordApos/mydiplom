<div class="row">
    <div class="col-md-12 home-page">
        <div id='calendar' style=''></div>
    </div>
    <!-- /.col -->
</div>
<script>

    $(document).ready(function() {

        $('#calendar').fullCalendar({
            defaultDate: '<?=date('Y-m-d');?>',
            editable: false,
            eventLimit: true, // allow "more" link when too many events
            header: {
                left: 'prev,next today add_event',
                center: 'title',
                right: 'month'
            },
            firstDay: 1,
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв.', 'Фев.', 'Март', 'Апр.', 'Май', 'Июнь', 'Июль', 'Авг.', 'Сент.', 'Окт.', 'Ноя.', 'Дек.'],
            dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
            dayNamesShort: ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'],
            buttonText: {
                prevYear: '&nbsp;&lt;&lt;&nbsp;',
                nextYear: '&nbsp;&gt;&gt;&nbsp;',
                today: 'Сегодня',
                month: 'Месяц',
                week: 'Неделя',
                day: 'День'
            },
            events: <?=$json?>
            //events: [
            //    {
            //        title: 'Сериалы',
            //        start: '2019-10-31'
            //    },
            //    {
            //        title: 'Рыбалка',
            //        start: '2019-10-07',
            //        end: '2019-10-10'
            //    },
            //    {
            //        title: 'Диплом',
            //        start: '<?//=date('Y-m-d');?>//'
            //    }
            //]
        });

    });

</script>