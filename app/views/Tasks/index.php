<div class="card">
    <div class="card-header">
        <h3 class="card-title">Все задачи</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
        </div>
    </div>
    <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
            <tr>
                <th style="width: 1%">
                    #
                </th>
                <th style="width: 20%">
                    Название
                </th>
                <th style="width: 30%">
                    Выполняет
                </th>
                <th>
                    «Дедлайн»
                </th>
                <th style="width: 8%" class="text-center">
                    Статус
                </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($tasks as $task):?>
                <tr>
                    <td>
                        <?=$task['id'];?>
                    </td>
                    <td>
                        <a>
                            <?=$task['title'];?>
                        </a>
                    </td>
                    <td>
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <?=$task['secondname'];?> <?=$task['firstname'];?>
                            </li>
                        </ul>
                    </td>
                    <td>
                        <?php $task['deadline'] = strtotime($task['deadline']);
                        $task['deadline'] = date('d.m.Y', $task['deadline']);?>
                        <?=$task['deadline'];?>
                    </td>
                    <td class="project-state">
                        <?php if ($task['status'] == 2):?>
                        <span class="badge badge-success">Завершено</span>
                        <?php elseif ($task['status'] == 1):?>
                        <span class="badge badge-warning">В процессе</span>
                        <?php else:?>
                        <span class="badge badge-dark">Не начата</span>
                        <?php endif;?>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>