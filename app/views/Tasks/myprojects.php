<div class="card">
    <div class="card-header">
        <h3 class="card-title">Мои задачи</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
        </div>
    </div>
    <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
            <tr>
                <th style="width: 1%">
                    #
                </th>
                <th style="width: 20%">
                    Название
                </th>
                <th>
                    «Дедлайн»
                </th>
                <th style="width: 20%" class="text-center">
                    Статус
                </th>
                <th style="width: 20%">

                </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($tasks as $task):?>
                <tr>
                    <td>
                        <?=$task['id'];?>
                    </td>
                    <td>
                        <a>
                            <?=$task['title'];?>
                        </a>
                    </td>
                    <td>
                        <?php $task['deadline'] = strtotime($task['deadline']);
                        $task['deadline'] = date('d.m.Y', $task['deadline']);?>
                        <?=$task['deadline'];?>
                    </td>
                    <td class="project-state">
                        <?php if ($task['status'] == 2):?>
                            <span class="badge badge-success">Завершено</span>
                        <?php elseif ($task['status'] == 1):?>
                            <span class="badge badge-warning">В процессе</span>
                        <?php else:?>
                            <span class="badge badge-dark">Не начата</span>
                        <?php endif;?>
                    </td>
                    <td class="project-actions text-right">
                        <a class="btn btn-primary btn-sm" href="/task/<?=$task['alias'];?>">
                            <i class="fas fa-folder">
                            </i>
                            Детально
                        </a>
                        <!--<a class="btn btn-info btn-sm" href="#">
                            <i class="fas fa-pencil-alt">
                            </i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="#">
                            <i class="fas fa-trash">
                            </i>
                            Delete
                        </a>-->
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>