<div class="task">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $task['title']; ?></h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Описание задачи</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
            </div>
        </div>
        <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
                    <div class="row">
                        <div class="col-12">
                            <div class="post">
                                <p>
                                    <?= $task['content']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
                    <?php if (!empty($atts)): ?>
                        <div class="atts__block">
                        <?php foreach ($atts as $att): ?>
                            <div class="atts__item"><?=$att['name'];?></div>
                        <?php endforeach;?>
                        </div>
                    <?php endif;?>
                    <?php if ($task['status'] != 2): ?>
                        <?php if(strtotime($task['deadline']) >= strtotime(date('Y-m-d'))):?>
                        <h3>До окончание дедлайна:</h3>
                        <div id="countdown" class="countdown">
                            <div class="countdown-number">
                                <span class="days countdown-time"></span>
                                <span class="countdown-text">Дней</span>
                            </div>
                            <div class="countdown-number">
                                <span class="hours countdown-time"></span>
                                <span class="countdown-text">Часов</span>
                            </div>
                            <div class="countdown-number">
                                <span class="minutes countdown-time"></span>
                                <span class="countdown-text">Минут</span>
                            </div>
                            <div class="countdown-number">
                                <span class="seconds countdown-time"></span>
                                <span class="countdown-text">Секунд</span>
                            </div>
                        </div>
                            <br>
                            <br>
                        <?php
                        else:?>
                            <?php
                            $newdate = $task['deadline'];
                            $newdate = strtotime($newdate);
                            $newdate = date('d.m.Y', $newdate);
                            ?>
                            <h3 class="enddeadline"><?=$newdate;?></h3>
                            <br>
                            <br>
                        <?php endif;?>

                        <h4>Статус</h4>
                        <form action="/task/<?=$task['alias'];?>" method="post">
                            <select class="col-6 task__changeStatus" name="status">
                                <option value="2" <?php if ($task['status'] == 2) echo 'selected';?>>Завершено</option>
                                <option value="1" <?php if ($task['status'] == 1) echo 'selected';?>>В процессе</option>
                                <option value="0" <?php if ($task['status'] == 0) echo 'selected';?>>Не начата</option>
                            </select>
                            <input class="col-3 task__submitStatus" type="submit" value="Сохранить">
                        </form>
                    <?php endif; ?>
                    <?php if ($task['status'] == 2): ?>
                        <span class="badge badge-success">Завершено</span>
                        <?php if (!empty($task['rating'])): ?>
                            <br>
                            <?php echo star($task['rating']); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>
<?php
$date = $task['deadline'];
$date = strtotime($date);
$date = date('F d Y', $date);
if ($task['status'] != 2): ?>
    <script>
        function getTimeRemaining(endtime)
        {
            var t = Date.parse(endtime) - Date.parse(new Date());
            var seconds = Math.floor((t / 1000) % 60);
            var minutes = Math.floor((t / 1000 / 60) % 60);
            var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
            var days = Math.floor(t / (1000 * 60 * 60 * 24));
            return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
            };
        }

        function initializeClock(id, endtime)
        {
            var clock = document.getElementById(id);
            var daysSpan = clock.querySelector('.days');
            var hoursSpan = clock.querySelector('.hours');
            var minutesSpan = clock.querySelector('.minutes');
            var secondsSpan = clock.querySelector('.seconds');

            function updateClock()
            {
                var t = getTimeRemaining(endtime);

                daysSpan.innerHTML = t.days;
                hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

                if (t.total <= 0)
                {
                    clearInterval(timeinterval);
                }
            }

            updateClock();
            var timeinterval = setInterval(updateClock, 1000);
        }

        var deadline = "<?=$date?> 23:59:59 GMT+0300"; //for Ukraine
        //var deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000); // for endless timer
        initializeClock('countdown', deadline);
    </script>
<?php endif; ?>