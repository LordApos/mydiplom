<?php

namespace app\models;
use RedBeanPHP\R;

class Edit extends AppModel
{
    public $attributes = [
        'password' => '',
        'firstname' => '',
        'secondname' => '',
        'email' => ''
    ];

    public $rules = [
        'required' => [
            ['firstname'],
            ['secondname'],
            ['email'],
        ],
        'email' => [
            ['email'],
        ],
        'lengthMin' => [
            ['password', 6],
        ]
    ];
}