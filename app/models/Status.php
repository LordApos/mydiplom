<?php

namespace app\models;

use RedBeanPHP\R;

class Status extends AppModel
{
    public $attributes = [
        'status' => '2',
    ];

    public $rules = [
        'required' => [
            ['status'],
        ],
    ];
}