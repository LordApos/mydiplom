<?php

namespace app\models\admin;

use app\models\AppModel;
use RedBeanPHP\R;

class Task extends AppModel
{
    public $attributes = [
        'title' => '',
        'content' => '',
        'status' => '',
        'deadline' => '',
        'alias' => '',
        'dev_id' => '',
        'rating' => '',
    ];

    public $rules = [
        'required' => [
            ['title'],
            ['content'],
            ['deadline'],
        ],
    ];

    public function editAtributs($id, $data){
        $filter = R::getCol('SELECT attribut_id FROM attributs_meta WHERE task_id = ?', [$id]);
        // если менеджер убрал фильтры - удаляем их
        if(empty($data['atts']) && !empty($filter)){
            R::exec("DELETE FROM attributs_meta WHERE task_id = ?", [$id]);
            return;
        }
        // если фильтры добавляются
        if(empty($filter) && !empty($data['atts'])){
            $sql_part = '';
            foreach($data['atts'] as $v){
                $sql_part .= "($v, $id),";
            }
            $sql_part = rtrim($sql_part, ',');
            R::exec("INSERT INTO attributs_meta (attribut_id, task_id) VALUES $sql_part");
            return;
        }
        // если изменились фильтры - удалим и запишем новые
        if(!empty($data['atts'])){
            $result = array_diff($filter, $data['atts']);
            if(!$result || count($filter) != count($data['atts'])){
                R::exec("DELETE FROM attributs_meta WHERE task_id = ?", [$id]);
                $sql_part = '';
                foreach($data['atts'] as $v){
                    $sql_part .= "($v, $id),";
                }
                $sql_part = rtrim($sql_part, ',');
                R::exec("INSERT INTO attributs_meta (attribut_id, task_id) VALUES $sql_part");
            }
        }
    }
}