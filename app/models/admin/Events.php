<?php

namespace app\models\admin;
use app\models\AppModel;
use RedBeanPHP\R;

class Events extends AppModel
{
    public $attributes = [
        'title' => '',
        'start' => '',
        'end' => '',
    ];

    public $rules = [
        'required' => [
            ['title'],
            ['start'],
        ],
    ];
}