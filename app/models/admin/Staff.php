<?php

namespace app\models\admin;
use app\models\AppModel;
use RedBeanPHP\R;

class Staff extends AppModel
{
    public $attributes = [
        'id' => '',
        'dev_id' => '',
    ];

    public $rules = [
        'required' => [
            ['id'],
            ['dev_id'],
        ],
    ];
}