<?php

namespace app\models\admin;
use app\models\AppModel;
use RedBeanPHP\R;

class Filter extends AppModel
{
    public $attributes = [
        'name' => '',
        'profession' => '',
    ];

    public $rules = [
        'required' => [
            ['name'],
            ['profession'],
        ],
    ];
}