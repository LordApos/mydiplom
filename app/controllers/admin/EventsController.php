<?php

namespace app\controllers\admin;
use RedBeanPHP\R;
use app\models\AppModel;
use app\models\admin\Events;

class EventsController extends AppController
{
    public function indexAction()
    {
        $events = R::findAll('events');
        $this->setMeta("Мероприятия");
        $this->set(compact('events'));
    }
    public function editAction()
    {
        $id = $this->getRequestID();
        $event = R::load('events', $id);
        if (!empty($_POST))
        {
            $event = new Events();
            $data = $_POST;
            $event->load($data);
            if(!$event->validate($data)){
                $event->getErrors();
                redirect();
            }
            if($event->update('events', $id)){
                $event = R::load('events', $id);
                R::store($event);
                $_SESSION['success'] = 'Изменения сохранены';
                redirect();
            }
        }
        $this->setMeta("Редактирование мероприятия {$event['title']}");
        $this->set(compact('event'));
    }

    public function addAction()
    {
        if(!empty($_POST)){
            $event = new Events();
            $data = $_POST;
            $event->load($data);

            if(!$event->validate($data)){
                $event->getErrors();
                $_SESSION['form_data'] = $data;
                redirect();
            }

            if($id = $event->save('events')){
                $event = R::load('events', $id);
                R::store($event);
                $_SESSION['success'] = 'Мероприятие добавлено';
            }
            redirect();
        }
        $this->setMeta('Добавление мероприятия');
    }

    public function deleteAction()
    {
        $id = $this->getRequestID();
        $event = R::load('events', $id);
        R::trash($event);
        $_SESSION['success'] = 'Мероприятие удалено';
        redirect();
    }
}