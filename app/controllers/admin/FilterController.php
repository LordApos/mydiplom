<?php

namespace app\controllers\admin;
use RedBeanPHP\R;
use app\models\AppModel;
use app\models\admin\Filter;

class FilterController extends AppController
{
    public function indexAction()
    {
        $attributs = R::findAll('attributs');
        $this->setMeta("Метки");
        $this->set(compact('attributs'));
    }
    public function editAction()
    {
        $id = $this->getRequestID();
        $attributs = R::load('attributs', $id);
        if (!empty($_POST))
        {
            $attributs = new Filter();
            $data = $_POST;
            $attributs->load($data);
            if(!$attributs->validate($data)){
                $attributs->getErrors();
                redirect();
            }
            if($attributs->update('attributs', $id)){
                $attributs = R::load('attributs', $id);
                R::store($attributs);
                $_SESSION['success'] = 'Изменения сохранены';
                redirect();
            }
        }
        $this->setMeta("Редактирование метки");
        $this->set(compact('attributs'));
    }

    public function addAction()
    {
        if(!empty($_POST)){
            $attributs = new Filter();
            $data = $_POST;
            $attributs->load($data);

            if(!$attributs->validate($data)){
                $attributs->getErrors();
                $_SESSION['form_data'] = $data;
                redirect();
            }

            if($id = $attributs->save('attributs')){
                $attributs = R::load('attributs', $id);
                R::store($attributs);
                $_SESSION['success'] = 'Метка добавлена';
            }
            redirect();
        }
        $this->setMeta('Добавление метки');
    }

    public function deleteAction()
    {
        $id = $this->getRequestID();
        $attributs = R::load('attributs', $id);
        R::trash($attributs);
        $_SESSION['success'] = 'Метка удалена';
        redirect();
    }
}