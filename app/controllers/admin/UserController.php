<?php

namespace app\controllers\admin;
use RedBeanPHP\R;
use app\models\admin\User;

class UserController extends AppController
{

    public function indexAction()
    {
        $users = R::findAll('user');
        $this->setMeta("Пользователи");
        $this->set(compact('users'));
    }

    public function editAction()
    {

        $id = $this->getRequestID();
        $user = R::load('user', $id);
        if (!empty($_POST))
        {
            $user = new User();
            $data = $_POST;
            $user->load($data);
            if(!$user->validate($data)){
                $user->getErrors();
                redirect();
            }
            if($user->update('user', $id)){
                $user = R::load('user', $id);
                R::store($user);
                $_SESSION['success'] = 'Изменения сохранены';
                redirect();
            }
        }
        $this->set(compact('user'));
    }

    public function deleteAction()
    {
        $id = $this->getRequestID();
        $user = R::load('user', $id);
        R::trash($user);
        $_SESSION['success'] = 'Пользователь удален';
        redirect();
    }

}