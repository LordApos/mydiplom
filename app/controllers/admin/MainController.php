<?php

namespace app\controllers\admin;

use RedBeanPHP\R;

class MainController extends AppController {

    public function indexAction(){
        $tasks = R::getAll("SELECT `task`.`id`, `task`.`status`, `task`.`deadline`, `task`.`alias`, `task`.`title`, `user`.`firstname`, `user`.`secondname` FROM `task`
          LEFT JOIN `user` ON `task`.`dev_id` = `user`.`id`
          ORDER BY `task`.`status`");
        $this->setMeta('Панель управления');
        $this->set(compact('tasks'));
    }

}