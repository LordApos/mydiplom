<?php

namespace app\controllers\admin;
use app\models\AppModel;
use RedBeanPHP\R;
use app\models\admin\Task;
class TaskController extends AppController
{

    public function editAction()
    {
        $id = $this->getRequestID();
        $task = R::load('task', $id);
        $attsAll = R::findAll('attributs');
        $devs = R::findAll('user');
        $selectedAtts = R::getAll("SELECT `att`.`name`, `att`.`id` FROM `attributs` AS `att`
          JOIN `attributs_meta` AS `att_meta` ON (`att_meta`.`attribut_id` = `att`.`id` AND `att_meta`.`task_id` = ?)
          ORDER BY `att`.`id`", ["{$task['id']}"]);
        $atts = [];
        $selected = [];
        foreach ($selectedAtts as $item)
        {
            $selected[] = $item['id'];
        }
        foreach ($attsAll as $item)
        {
            if (in_array($item['id'], $selected))
            {
                $item['status'] = '1';
            }
            else
            {
                $item['status'] = '0';
            }
            $atts[] = $item;
        }
        if (!empty($_POST))
        {
            $task = new Task();
            $data = $_POST;
            $task->load($data);
            if(!$task->validate($data)){
                $task->getErrors();
                redirect();
            }
            if($task->update('task', $id)){
                $task->editAtributs($id, $data);
                $alias = AppModel::createAlias('task', 'alias', $data['title'], $id);
                $task = R::load('task', $id);
                $task->alias = $alias;
                R::store($task);
                $_SESSION['success'] = 'Изменения сохранены';
                redirect();
            }
        }
        $this->setMeta("Редактирование задачи {$task['title']}");
        $this->set(compact('task', 'atts', 'devs'));
    }

    public function addAction()
    {
        if(!empty($_POST)){
            $task = new Task();
            $data = $_POST;
            $task->load($data);

            if(!$task->validate($data)){
                $task->getErrors();
                $_SESSION['form_data'] = $data;
                redirect();
            }

            if($id = $task->save('task')){
                $task->editAtributs($id, $data);
                $alias = AppModel::createAlias('task', 'alias', $data['title'], $id);
                $p = R::load('task', $id);
                $p->alias = $alias;
                R::store($p);
                $_SESSION['success'] = 'Задача добавлена';
            }
            redirect();
        }
        $atts = R::findAll('attributs');
        $devs = R::findAll('user');
        $this->setMeta('Новая задача');
        $this->set(compact('atts', 'devs'));
    }

    public function deleteAction()
    {
        $id = $this->getRequestID();
        $task = R::load('task', $id);
        R::trash($task);
        $_SESSION['success'] = 'Задача удалена';
        redirect();
    }

}