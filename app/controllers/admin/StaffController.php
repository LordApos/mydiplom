<?php

namespace app\controllers\admin;
use app\models\admin\Staff;
use RedBeanPHP\R;

class StaffController extends AppController
{
    public function indexAction()
    {
        $tasks = R::getAll("SELECT `task`.`id`, `task`.`status`, `task`.`deadline`, `task`.`title` FROM `task`
          WHERE `task`.`status` = '0' AND `task`.`dev_id` = 'NULL'");
        if (!empty($_POST))
        {
            $taskID = $_POST['task'];
            $selectedAtts = R::getAll("SELECT `att`.`name`, `att`.`profession`, `att`.`id` FROM `attributs` AS `att`
          JOIN `attributs_meta` AS `att_meta` ON (`att_meta`.`attribut_id` = `att`.`id` AND `att_meta`.`task_id` = ?)
          ORDER BY `att`.`id`", ["{$taskID}"]);
            $atts = [];
            foreach ($selectedAtts as $item)
            {
                $atts[] = $item['id'];
            }
            $profession = reset($selectedAtts);
            $profession = $profession['profession'];
            $users = R::getAll("SELECT `user`.`id`, `user`.`firstname`, `user`.`secondname`, `user`.`raiting` FROM `user` WHERE `user`.`profession` = ?", ["{$profession}"]);
            $usersRaiting = [];
            foreach ($users as $user)
            {
                $raitingAtt = 0;
                $attsTasks = R::getAll("SELECT `task`.`rating`, `att`.`name`, `att`.`profession`, `att`.`id` FROM `attributs` AS `att`
          JOIN `attributs_meta` AS `att_meta` ON (`att_meta`.`attribut_id` = `att`.`id`)
          JOIN `task` ON (`task`.`id`=`att_meta`.`task_id` AND `task`.`dev_id` = ? and `task`.`status` = '2')", ["{$user['id']}"]);
                foreach ($attsTasks as $item)
                {
                    $raitingAtt += $item['rating'];
                }
                $activeTask = R::getAll("SELECT `task`.`id` FROM `task` WHERE `task`.`dev_id` = ? AND (`task`.`status` = '1' OR `task`.`status` = '0')", ["{$user['id']}"]);
                $activeTask = count($activeTask);
                $raitingAtt = $raitingAtt + $user['raiting'] * 5;
                $user['allraiting'] = $raitingAtt;
                $user['activetask'] = $activeTask;
                $usersRaiting[] = $user;
            }
        }

        $this->setMeta("Подбор персонала");
        $this->set(compact('tasks', 'usersRaiting', 'taskID'));
    }

    public function editAction()
    {
        $id = $_POST['id'];
        if (!empty($_POST))
        {
            $task = new Staff();
            $data = $_POST;
            $task->load($data);
            if(!$task->validate($data)){
                $task->getErrors();
                redirect();
            }
            if($task->update('task', $id)){
                $task = R::load('task', $id);
                R::store($task);
                $_SESSION['success'] = 'Изменения сохранены';
                redirect('/admin/task/edit?id='.$id);
            }
        }
        $this->setMeta("Редактирование");
    }
}