<?php

namespace app\controllers;

use RedBeanPHP\R;

class MainController extends AppController {

    public function indexAction(){
        $title = 'Главная';
        $events = R::findAll('events');
        $json = [];
        foreach ($events as $event)
        {
            if ($event['end'] == null)
            {
                unset($event['end']);
            }
            $json[] = $event;
        }
        $json = json_encode($json);
        $this->setMeta('Главная страница', 'Описание...', 'Ключевики...');
        $this->set(compact('title', 'json'));
    }

}