<?php

namespace app\controllers;

use app\models\Edit;
use app\models\User;
use RedBeanPHP\R;

class UserController extends AppController
{

    public function signupAction()
    {
        $this->layout = 'login';
        if (!empty($_POST))
        {
            $user = new User();
            $data = $_POST;
            $user->load($data);
            if (!$user->validate($data) || !$user->checkUnique())
            {
                $user->getErrors();
                $_SESSION['form_data'] = $data;
            }
            else
            {
                $user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);
                if ($user->save('user'))
                {
                    $_SESSION['success'] = 'Пользователь зарегистрирован';
                    redirect('/user/login');
                }
                else
                {
                    $_SESSION['error'] = 'Ошибка!';
                }
            }
            redirect();
        }
        $this->setMeta('Регистрация');
    }

    public function loginAction()
    {
        $this->layout = 'login';
        if (!empty($_POST))
        {
            $user = new User();
            if ($user->login())
            {
                redirect('/');
            }
            else
            {
                $_SESSION['error'] = 'Логин/пароль введены неверно';
            }
            redirect();
        }
        $this->setMeta('Вход');
    }

    public function logoutAction()
    {
        if (isset($_SESSION['user']))
        {
            unset($_SESSION['user']);
        }
        redirect();
    }

    public function cabinetAction()
    {
        if (!User::checkAuth())
        {
            redirect();
        }
        $user = R::findOne('user', "id = ?", [$_SESSION['user']['id']]);
        $atts = R::getAll("SELECT `att`.`name` FROM `attributs` AS `att`
          JOIN  `task` ON  `task`.`dev_id` = ?
          JOIN `attributs_meta` AS `att_meta` ON (`att_meta`.`attribut_id` = `att`.`id` AND `att_meta`.`task_id` = `task`.`id`)
          ORDER BY `att`.`id`", ["{$user['id']}"]);
        $tasks = R::getAll("SELECT `task`.`status` FROM `task`
          WHERE `task`.`dev_id` = ?", ["{$user['id']}"]);
        $alltasks = count($tasks);
        $activetasks = 0;
        $finishtasks = 0;
        if ($alltasks == 0 || empty($alltasks))
        {
            $alltasks = 0;
        }
        else
        {
            foreach ($tasks as $task)
            {
                if ($task['status'] == 1)
                {
                    $activetasks += 1;
                }
                elseif ($task['status'] == 2)
                {
                    $finishtasks += 1;
                }
            }
        }
        $this->setMeta('Личный кабинет');
        $this->set(compact('user', 'atts', 'alltasks', 'activetasks', 'finishtasks'));
    }

    public function editAction()
    {
        if (!User::checkAuth())
        {
            redirect('/user/login');
        }
        if (!empty($_POST))
        {
            $user = new Edit();
            $data = $_POST;
            $data['id'] = $_SESSION['user']['id'];
            $user->load($data);
            if (!$user->attributes['password'])
            {
                unset($user->attributes['password']);
            }
            else
            {
                $user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);//$2y$10$Qqcovme9oUcr4AUqVjRbGOfgocGVDAEEnXYGBBKoyTgh2AGC1UAxy
            }
            if (!$user->validate($data))
            {
                $user->getErrors();
                redirect();
            }
            if ($user->update('user', $_SESSION['user']['id']))
            {
                foreach ($user->attributes as $k => $v)
                {
                    if ($k != 'password')
                    {
                        $_SESSION['user'][$k] = $v;
                    }
                }
                $_SESSION['success'] = 'Изменения сохранены';
            }
            redirect();
        }
        $this->setMeta('Изменение личных данных');
    }
}