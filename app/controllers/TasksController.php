<?php

namespace app\controllers;

use RedBeanPHP\R;

class TasksController extends AppController
{
    public function indexAction()
    {
        $tasks = R::getAll("SELECT `task`.`id`, `task`.`status`, `task`.`deadline`, `task`.`title`, `user`.`firstname`, `user`.`secondname` FROM `task`
          JOIN `user` ON `task`.`dev_id` = `user`.`id`
          ORDER BY `task`.`status`");
        $this->setMeta('Все задачи', 'Описание...', 'Ключевики...');
        $this->set(compact('tasks'));
    }

    public function myprojectsAction()
    {
        $tasks = R::getAll("SELECT `task`.`id`, `task`.`alias`, `task`.`status`, `task`.`deadline`, `task`.`title` FROM `task`
          WHERE `task`.`dev_id` = ?
          ORDER BY `task`.`status`", ["{$_SESSION['user']['id']}"]);
        $this->setMeta('Мои задачи', 'Описание...', 'Ключевики...');
        $this->set(compact('tasks'));
    }
}