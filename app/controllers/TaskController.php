<?php

namespace app\controllers;

use RedBeanPHP\R;

class TaskController extends AppController
{

    public function viewAction()
    {
        $alias = $this->route['alias'];
        $task = R::findOne('task', "alias = ?", [$alias]);
        $atts = R::getAll("SELECT `att`.`name` FROM `attributs` AS `att`
          LEFT JOIN `attributs_meta` AS `att_meta` ON (`att_meta`.`attribut_id` = `att`.`id` AND `att_meta`.`task_id` = ?)
          ORDER BY `att`.`id`", ["{$task['id']}"]);
        if (!empty($_POST))
        {
            $status = new \app\models\Status();
            $data = $_POST;
            $status->load($data);
            if($status->update('task', $task['id'])){
                $_SESSION['success'] = 'Изменения сохранены';
                redirect();
            }
        }

        if(!$task){
            throw new \Exception('Страница не найдена', 404);
        }
        $this->setMeta($task->title, 'description', 'keywords');
        $this->set(compact('task', 'atts'));
    }

}