<?php

namespace app\controllers;

use app\models\AppModel;
use app\models\User;
use ishop\base\Controller;

class AppController extends Controller{

    public function __construct($route){
            if(!User::checkAuth())
            {

                if ($_SERVER['REQUEST_URI'] == '/user/login')
                {
                }
                elseif($_SERVER['REQUEST_URI'] == '/user/signup')
                {
                }
                else
                {
                    redirect('/user/login');
                }
            }
        parent::__construct($route);
        new AppModel();
    }
}