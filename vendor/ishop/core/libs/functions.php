<?php

function debug($arr, $die = false){
    echo '<pre>' . print_r($arr, true) . '</pre>';
    if($die) die;
}

function redirect($http = false){
    if($http){
        $redirect = $http;
    }else{
        $redirect = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : PATH;
    }
    header("Location: $redirect");
    exit;
}

function h($str){
    return htmlspecialchars($str, ENT_QUOTES);
}

function star($int)
{
    if (($int - 5) != 0)
    {
        $output = '';
        $minus = 5 - $int;
        $plus = 5 - $minus;
        for ($i = 0; $i < $plus; $i++)
        {
            $output .= '<i class="fas fa-star"></i>';
        }
        for ($i = 0; $i < $minus; $i++)
        {
            $output .= '<i class="far fa-star"></i>';
        }
    }
    else
    {
        $output = '<i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>';
    }
    return $output;
}