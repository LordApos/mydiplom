$( document ).ready(function() {
    $('.nav-sidebar a').each(function(){
        var location = window.location.protocol + '//' + window.location.host + window.location.pathname;
        var link = this.href;
        if(link == location){
            $(this).addClass('active');
            $(this).closest('.has-treeview').addClass('active');
            $(this).closest('.has-treeview').addClass('menu-open');
        }
    });

    $('.devs__item label').click(function ()
    {
       $(this).parent().toggleClass('active');
    });

});